#include <iostream>


using namespace std;

void programStart(string start, bool &program_run);
void greeting(double &account_balance, double &overdrawn_amount);
void overdraftCalculation(double account_balance,
                          double flat_fee,
                          double fee_variable,
                          double overdrawn_amount);
void endModule(bool &program_run);

int main() {
    double fee_per_overdraft = 5;
    double flat_fee = .01;
    bool program_run;
    string start;

    programStart(start, program_run);
    while (program_run) {
        double account_balance;
        double overdrawn_amount;
        greeting(account_balance, overdrawn_amount);
        overdraftCalculation(account_balance, flat_fee, fee_per_overdraft, overdrawn_amount);
        endModule(program_run);
    }
    return 0;
}

void programStart(string start, bool &program_run) {
    cout << "Enter: 'run'" << endl;
    cin >> start;
    if (start == "run") {
        program_run = true;
    } else program_run = false;
}

void greeting(double &account_balance, double &overdrawn_amount) {
    cout << "Account Balance? $";
    cin >> account_balance;
    cout << "Overdrawn Count: #";
    cin >> overdrawn_amount;
}

void overdraftCalculation(double account_balance, double flat_fee, double fee_variable, double overdrawn_amount) {
    double new_balance;
    double end_fee_charged;
    double end_overdrawn_charged;
    double end_fees_total;

    end_fee_charged = account_balance * flat_fee;
    end_overdrawn_charged = overdrawn_amount * fee_variable;
    end_fees_total = end_fee_charged + end_overdrawn_charged;
    new_balance = account_balance - end_fees_total;

    cout << "\nSTARTING BALANCE: $" << account_balance << endl;
    cout << (flat_fee * 100) << "% OVERDRAFT FEE: $" << end_fee_charged << endl;
    cout << "TOTAL OVERDRAFTS: #" << overdrawn_amount << endl;
    cout << "OVERDRAFT CHARGES: $" << end_overdrawn_charged << endl;
    cout << "\nTOTAL FEES: $" << end_fees_total << endl;
    cout << "\nNEW BALANCE: $" << new_balance << endl;
}

void endModule(bool &program_run) {
    string user_answer;
    cout << "would you like to continue" << endl;
    cin >> user_answer;
    if (user_answer != "yes") {
        program_run = false;
        cout << "Thanks you for running me!!\nHave a great day" << endl;
    } else program_run = true;
}
